#!groovy
import static groovy.json.JsonOutput.*

def config =
    [
        pimUrl: 'https://api.ugfischer.com/',
        branches:
        [
            'instance/test':
            [
                'pim2factfinderGetFromRepository': true,
                'pim2factfinderJarPath': 'pim-to-factfinder.jar',
                'pim2factfinderVersion': '1.3.0-SNAPSHOT',
                'gitCredentialsId': 'sc-fischer__bitbucket.org',
                'baseDataDir': '/data/fischer/test',
                // First FACT-Finder node will be used for import
                'factfinderNodeUrls': ['https://ff-test.smartcommerce.se/fact-finder/'],
                'catalogrestservicesNodeUrls': ['https://fischer-sitecore-cat.smartcommerce.se'],
                'notificationsTo': 'fischer@smartcommerce.biz',
                'office365HookUrl': 'https://outlook.office.com/webhook/f879555f-8fd6-480f-a684-06f2179301d3@c9d5f7f8-284b-4cbb-9b7f-fdc3dd75620c/JenkinsCI/225c046b04954070a4ad5d0e7cb5c7ed/4a2b5ccd-a27f-4707-9804-6c881f56de9b'
            ],
            'instance/production':
            [
                'pim2factfinderGetFromRepository': false,
                'pim2factfinderJarPath': '/share/pim-to-factfinder/pim-to-factfinder.jar',
                'pim2factfinderVersion': '',
                'gitCredentialsId': 'sc-fischer__bitbucket.org',
                'baseDataDir': '/share/data',
                 // First FACT-Finder node will be used for import
                'factfinderNodeUrls': ['http://10.208.128.192:8080/fact-finder/', 'http://10.208.128.190:8080/fact-finder/', 'http://10.208.128.191:8080/fact-finder/'],
                'catalogrestservicesNodeUrls': ['http://10.208.128.190:9090', 'http://10.208.128.191:9090'],
                'notificationsTo': 'fischer@smartcommerce.biz',
                'office365HookUrl': 'https://outlook.office.com/webhook/f879555f-8fd6-480f-a684-06f2179301d3@c9d5f7f8-284b-4cbb-9b7f-fdc3dd75620c/JenkinsCI/225c046b04954070a4ad5d0e7cb5c7ed/4a2b5ccd-a27f-4707-9804-6c881f56de9b'
            ]
        ]
    ]
def getChannelConfig
def channelConfig
def channelResult = [:]

def processChannels(Map config, Map channelConfig, String branch, Map channelResult)
{
    def channelIds
    if (params.channels == 'automatic') {
        channelIds = channelConfig.findAll { it.value.automatic }.collect { it.key }
    }
    else if (params.channels == 'all') {
        channelIds = channelConfig.keySet()
    }
    else {
        channelIds = [ params.channels ]
    }
    echo "Processing channels: ${channelIds}"

    currentBuild.displayName = "#${env.BUILD_NUMBER} - (${params.channels}): ${channelIds}"
    currentBuild.description = "Channels parameter: ${params.channels}\nChannels: ${channelIds}"

    def channels =  channelConfig.findAll { it.key in channelIds }.collect { it.value }

    def builders = [:]

    for (c in channels) {
        def channel = c

        builders["${channel['brand']}-${channel['locale']}"] = {
            processSingleChannel(config, branch, channel, channelResult)
        }

        channelResult["${channel['brand']}-${channel['locale']}"] = "FAILURE"
    }
    parallel builders

    echo "Parallel transformation completed"
}

def processSingleChannel(Map config, String branch, Map channel, Map channelResult) {
    node('fischer-job') {
        script {
            def brand = channel['brand']
            def locale = channel['locale']

            if (config['branches'][branch]['pim2factfinderGetFromRepository']) {
                unstash name: "pim-to-factfinder-jar"
            }

            withCredentials([usernamePassword(credentialsId: 'pim_fischer', passwordVariable: 'pimPassword', usernameVariable: 'pimUser')]) {
                def parameters = pim2factfinderParameter(config, branch, channel, pimUser, pimPassword)

                sh "java -jar ${config['branches'][branch]['pim2factfinderJarPath']} ${parameters}"
            }
            echo "Processing data of ${brand}-${locale} completed"

            withCredentials([usernamePassword(credentialsId: 'ff_autoimport', passwordVariable: 'ffiPassword', usernameVariable: 'ffiUser')]) {
                def ffImport = config['branches'][branch]['factfinderNodeUrls'][0]

                retry(3) {
                    def importResponse = httpRequest validResponseCodes: '200', url: "${ffImport}Import.ff?username=${ffiUser}&password=${ffiPassword}&channel=${brand}-${locale}-catalog&format=json&verbose=true"
                    echo("FACT-Finder: Response from ${ffImport}Import.ff" + importResponse.content)
                }

                for (ff in config['branches'][branch]['factfinderNodeUrls']) {
                    retry(3) {
                        def reloadResponse = httpRequest httpMode: 'POST', validResponseCodes: '200', url: "${ff}RefreshDatabases.ff?username=${ffiUser}&password=${ffiPassword}&channel=${brand}-${locale}-catalog&format=json&do=refreshAllDatabases"
                        echo("FACT-Finder: Response from ${ff}RefreshDatabases.ff " + reloadResponse.content)
                    }
                }
            }
            echo "Triggering FACT-Finder import and refresh database completed"

            for (crs in config['branches'][branch]['catalogrestservicesNodeUrls']) {
                retry(3) {
                    def response = httpRequest httpMode: 'PUT', validResponseCodes: '204', url: "${crs}/v1/configurations/${brand}-${locale}"
                    echo ("Catalog REST Services: Response from ${crs} " + response)
                }
            }
            echo "Triggering Catalog REST Services config reload completed"

            channelResult["${brand}-${locale}"] = "SUCCESS"
        }
    }
}

def String pim2factfinderParameter(Map config, String branch, Map channel, String user, String password) {
    def brand = channel.find { it.key == 'brand' }?.value
    def locale = channel.find { it.key == 'locale' }?.value

    def pimUrl = config.find { it.key == 'pimUrl' }?.value
    def route = channel.find { it.key == 'route' }?.value
    def assortmentId = channel.find { it.key == 'assortmentId' }?.value
    def language = channel.find { it.key == 'language' }?.value

    def productInputFile = channel.find { it.key == 'products' }?.value ? "${config['branches'][branch]['baseDataDir']}/fischer-data-input/${channel['products']}" : null
    def categoryInputFile = channel.find { it.key == 'categories' }?.value ? "${config['branches'][branch]['baseDataDir']}/fischer-data-input/${channel['categories']}" : null
    def retailerInputFile = channel.find { it.key == 'retailers' }?.value ? "${config['branches'][branch]['baseDataDir']}/fischer-data-input/${channel['retailers']}" : null

    def exportFile = "${config['branches'][branch]['baseDataDir']}/factfinder/export.${brand}-${locale}-catalog.csv"
    def catalogRestServicesFolder = "${config['branches'][branch]['baseDataDir']}/catalogrestservices/"

    echo """Parameters:
           |===========
           |brand: ${brand}
           |locale: ${locale}
           |catalogRestServicesFolder: ${catalogRestServicesFolder}
           |exportFile: ${exportFile}
           |user: ${user}
           |password: ${password}
           |pimUrl: ${pimUrl}
           |route: ${route}
           |assortmentId: ${assortmentId}
           |language: ${language}
           |productInputFile: ${productInputFile}
           |categoryInputFile: ${categoryInputFile}
           |retailerInputFile: ${retailerInputFile}
           |""".stripMargin()

    if (brand==null || locale==null || exportFile==null || catalogRestServicesFolder==null ) {
        return null
    }

    def parameters = "${brand} -l=${locale} -c=${catalogRestServicesFolder} -f=${exportFile}"
    if (user && password && pimUrl && route && assortmentId && language ) {
        parameters = parameters + " -u=${user} -p=${password} -i=${pimUrl} -r=${route} -a=${assortmentId} -m=${language}"
    }
    if (productInputFile && categoryInputFile ) {
        parameters = parameters + " -p=${productInputFile} -k=${categoryInputFile}"
    }
    if (retailerInputFile) {
        parameters = parameters + " -e=${retailerInputFile}"
    }

    return parameters
}

def sendNotifications(Object build, String additionalTo, String office365HookUrl, Map channelResult) {
    def colors = ['STARTED': [
                    name: 'YELLOW',
                    code: '#FFFF00'
                  ],
                  'SUCCESS': [
                    name: 'GREEN',
                    code: '#00FF00'
                  ],
                  'WARNING': [
                    name: 'YELLOW',
                    code: '#FFFF00'
                  ],
                  'FAILED': [
                    name: 'RED',
                    code: '#FF0000'
                  ]
                 ]

    // build result of null means successful
    buildStatus = build.result ?: 'SUCCESS'

    def duration = (System.currentTimeMillis() - build.startTimeInMillis) / 1000

    def subject = "SearchBackend Data Update: ${buildStatus} (Job ${env.JOB_NAME}, ${build.displayName})"

    def channelTable = """<table border="1">
                         |  <tr>
                         |    <th>Channel</th>
                         |    <th>Result</th>
                         |  </tr>
                         |""".stripMargin()
    for (channel in channelResult) {
        channelTable = channelTable +
                         """  <tr>
                           |    <td style="font-weight:bold;">${channel.key}</td>
                           |    <td style="background-color:${colors[channel.value].code};color:black;">${channel.value}</td>
                           |  </tr>
                           |""".stripMargin()
    }
    channelTable = channelTable + "</table>"

    def details = """${channelTable}
                    |<p>Build duration: ${duration} seconds</p>
                    |<p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>""".stripMargin()

    def notificationBody = """<p>${subject}</p>
                             |${details}""".stripMargin()

    emailext (
        subject: subject,
        body: details,
        attachLog: false,
        to: additionalTo,
        recipientProviders: [
                                [$class: 'DevelopersRecipientProvider'],
                                [$class: 'RequesterRecipientProvider'],
                                [$class: 'FailingTestSuspectsRecipientProvider'],
                                [$class: 'FirstFailingBuildSuspectsRecipientProvider'],
                                [$class: 'CulpritsRecipientProvider'],
                                [$class: 'UpstreamComitterRecipientProvider']
                            ]
    )

    if (office365HookUrl) {
        office365ConnectorSend message: notificationBody, status: buildStatus, webhookUrl: office365HookUrl
    }
}

pipeline {
    agent {
        label 'fischer-job'
    }

    triggers {
        cron('00 20 * * *')
    }

    parameters {
        choice( name: 'channels',
                choices: "automatic\nall\nFIWE-de_AT\nFIWE-de_DE\nFIWE-en_AE\nFIWE-en_GB\nFIWE-en_ZZ\nFIWE-es_ES\nFIWE-it_IT\nFIWE-nl_NL\nFIWE-pl_PL\nFITE-de_DE\nFITE-en_GB\nFITIP-de_DE\nFITIP-en_GB",
                description: "Channels which will be imported.\nautomatic: All channels with the automatic flag\nall: All configured channels")
    }

    options {
        overrideIndexTriggers(false)
        timestamps()
        buildDiscarder(
            logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '180', numToKeepStr: '')
        )
    }

    stages {
        stage('Initialize') {
            steps {
                script {
                    echo "Executing Job on branch ${env.BRANCH_NAME}"
                    currentBuild.displayName = "#${env.BUILD_NUMBER} - (${params.channels})"

                    def channelConfigFile = env.BRANCH_NAME.replace("/", "_") + "_channelconfig.groovy"
                    echo "Loading channel configuration from ${channelConfigFile}"
                    getChannelConfig = load(channelConfigFile)
                    channelConfig = getChannelConfig()
                    prettyChannelConfig = prettyPrint(toJson(channelConfig))
                    echo "Channel Configuration:\n ${prettyChannelConfig}"

                    prettyConfig = prettyPrint(toJson(config))
                    echo "Configuration:\n ${prettyConfig}"

                    if (!config['branches'][env.BRANCH_NAME]) {
                        currentBuild.result = 'ABORTED'
                        error('No branch configuration available - aborting job')
                    }
                }
            }
        }

        stage('Download PIM-to-FACTFinder JAR') {
            agent {
                label 'maven'
            }

            tools {
                jdk 'JDK-DEFAULT'
                maven 'Maven-DEFAULT'
            }

            steps {
                script {
                    if (config['branches'][env.BRANCH_NAME]['pim2factfinderGetFromRepository']) {
                        configFileProvider([configFile(fileId: 'default_global_maven_settings.xml', variable: 'MAVEN_GLOBAL_SETTINGS')]) {
                            sh "mvn -B -gs $MAVEN_GLOBAL_SETTINGS org.apache.maven.plugins:maven-dependency-plugin:get -Dartifact=de.fischer:pim-to-factfinder:${config['branches'][env.BRANCH_NAME]['pim2factfinderVersion']}:jar:jar-with-dependencies -Dtransitive=false -Ddest=${config['branches'][env.BRANCH_NAME]['pim2factfinderJarPath']} -DrepoUrl=https://repository.smartcommerce.se/repository/maven-snapshots/"
                        }
                        stash name: "pim-to-factfinder-jar", includes: "*.jar"
                    }
                    else {
                        echo "Download of PIM-to-FACTFinder JAR skipped"
                    }
                }
            }
        }

        stage('Get input data from repository') {
            steps {
                script {
                    def inputDir = "${config['branches'][env.BRANCH_NAME]['baseDataDir']}/fischer-data-input"
                    dir (inputDir) {
                        git url: 'https://bitbucket.org/smartcommerce_se/fischer-data-input.git', branch: env.BRANCH_NAME, credentialsId: config['branches'][env.BRANCH_NAME]['gitCredentialsId'], poll: false
                    }
                }
            }
        }

        stage('Get and transform PIM data') {
            tools {
                jdk 'JDK-DEFAULT'
            }

            steps {
                script {
                    processChannels(config, channelConfig, env.BRANCH_NAME, channelResult)
                }
            }
        }
    }

    post {
        always {
            script {
                sendNotifications(currentBuild, config['branches'][env.BRANCH_NAME]['notificationsTo'], config['branches'][env.BRANCH_NAME]['office365HookUrl'], channelResult)
            }
        }
    }
}