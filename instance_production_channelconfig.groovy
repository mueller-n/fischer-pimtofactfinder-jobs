#!/usr/bin/env groovy

def Map call() {
    return [
        'FIWE-de_AT':
        [
            locale: 'de_AT',
            brand: 'FIWE',
            route: 'fischer',
            assortmentId: '1001020740',
            language: 'AT',
            retailers: 'retailer/retailer-FIWE-de_AT.csv',
            automatic: true
        ],
        'FIWE-de_DE':
        [
            locale: 'de_DE',
            brand: 'FIWE',
            route: 'fischer-v2',
            assortmentId: '1001341135',
            language: 'DE',
            retailers: 'retailer/retailer-FIWE-de_DE.csv',
            automatic: true
        ],
        'FIWE-en_AE':
        [
            locale: 'en_AE',
            brand: 'FIWE',
            route: 'fischer-v2',
            assortmentId: '1001378955',
            language: 'GB',
            retailers: 'retailer/retailer-FIWE-en_GB.csv',
            automatic: false
        ],
        'FIWE-en_GB':
        [
            locale: 'en_GB',
            brand: 'FIWE',
            route: 'fischer',
            assortmentId: '1001037602',
            language: 'GB',
            retailers: 'retailer/retailer-FIWE-en_GB.csv',
            automatic: true
        ],
        'FIWE-en_ZZ':
        [
            locale: 'en_ZZ',
            brand: 'FIWE',
            route: 'fischer-v2',
            assortmentId: '1001365939',
            language: 'GB',
            retailers: 'retailer/retailer-FIWE-en_ZZ.csv',
            automatic: false
        ],
        'FIWE-es_ES':
        [
            locale: 'es_ES',
            brand: 'FIWE',
            route: 'fischer',
            assortmentId: '1001023441',
            language: 'ES',
            retailers: 'retailer/retailer-FIWE-es_ES.csv',
            automatic: false
        ],
        'FIWE-it_IT':
        [
            locale: 'it_IT',
            brand: 'FIWE',
            route: 'fischer',
            assortmentId: '1001021292',
            language: 'IT',
            retailers: 'retailer/retailer-FIWE-en_GB.csv',
            automatic: false
        ],
        'FIWE-nl_NL':
        [
            locale: 'nl_NL',
            brand: 'FIWE',
            route: 'fischer-v2',
            assortmentId: '1001037170',
            language: 'NL',
            retailers: 'retailer/retailer-FIWE-en_GB.csv',
            automatic: false
        ],
        'FIWE-pl_PL':
        [
            locale: 'pl_PL',
            brand: 'FIWE',
            route: 'fischer',
            assortmentId: '1001086456',
            language: 'PL',
            retailers: 'retailer/retailer-FIWE-en_GB.csv',
            automatic: false
        ],
        'FITE-de_DE':
        [
            locale: 'de_DE',
            brand: 'FITE',
            products: 'catalog/fischertechnik-products.csv',
            categories: 'catalog/fischertechnik-categories.csv',
            retailers: 'retailer/retailer-FITE-de_DE.csv',
            automatic: true
        ],
        'FITE-en_GB':
        [
            locale: 'en_GB',
            brand: 'FITE',
            products: 'catalog/fischertechnik-products-en.csv',
            categories: 'catalog/fischertechnik-categories-en.csv',
            retailers: 'retailer/retailer-FITE-de_DE.csv',
            automatic: true
        ],
        'FITIP-de_DE':
        [
            locale: 'de_DE',
            brand: 'FITIP',
            products: 'catalog/fischertip-products.csv',
            categories: 'catalog/fischertip-categories.csv',
            retailers: 'retailer/retailer-FITE-de_DE.csv',
            automatic: true
        ],
        'FITIP-en_GB':
        [
            locale: 'en_GB',
            brand: 'FITIP',
            products: 'catalog/fischertip-products-en.csv',
            categories: 'catalog/fischertip-categories-en.csv',
            retailers: 'retailer/retailer-FITE-de_DE.csv',
            automatic: true
        ]
    ]
}

return this